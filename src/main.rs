//! This example powers on the first available controller
//! and then starts searching for devices.
//!
//! Copyright (c) 2020 Ibiyemi Abiodun
#[macro_use]
extern crate bluez;
extern crate paho_mqtt as mqtt;

use std::error::Error;
use std::time::Duration;

use bytes::Bytes;

use async_std::task::{block_on, sleep};

use bluez::client::*;
use bluez::interface::controller::*;
use bluez::interface::event::Event;
use std::convert::TryFrom;
use structopt::StructOpt;
use std::process;
use mqtt::Client;

use serde::{Deserialize, Serialize};
use serde_json::Result;
use mqtt::PropertyType::Byte;

// Define the qos.
const QOS:i32 = 1;

#[derive(Serialize)]
struct MQTTDiscoveryMessage {
    name: String,
    device_class: String,
    state_topic: String,
    unit_of_measurement: String,
    value_template: String,
    unique_id: String
}

#[derive(Serialize)]
struct DataMessage {
    temperature: f32,
    humidity: f32,
    rssi: i8
}

struct BTField {
    field_type: u8,
    data: Bytes
}

fn send_discover_message(cli: &Client, device: &String) {
    let temperature_message = MQTTDiscoveryMessage {
        name: device.parse().unwrap(),
        device_class: "temperature".to_string(),
        state_topic: format!("homeassistant/sensor/{}/state", device).to_string(),
        unit_of_measurement: "°C".to_string(),
        value_template: "{{ value_json.temperature }}".to_string(),
        unique_id: format!("{}_temperature", device)
    };

    let humidity_message = MQTTDiscoveryMessage {
        name: device.parse().unwrap(),
        device_class: "humidity".to_string(),
        state_topic: format!("homeassistant/sensor/{}/state", device).to_string(),
        unit_of_measurement: "%".to_string(),
        value_template: "{{ value_json.humidity }}".to_string(),
        unique_id: format!("{}_humidity", device)
    };

    let signal_strength = MQTTDiscoveryMessage {
        name: device.parse().unwrap(),
        device_class: "signal_strength".to_string(),
        state_topic: format!("homeassistant/sensor/{}/state", device).to_string(),
        unit_of_measurement: "db".to_string(),
        value_template: "{{ value_json.rssi }}".to_string(),
        unique_id: format!("{}_rssi", device)
    };

    let temperature_msg = mqtt::MessageBuilder::new()
        .topic(format!("homeassistant/sensor/{}/temperature/config", device))
        .payload(serde_json::to_string(&temperature_message).unwrap())
        .qos(1)
        .finalize();

    let humidity_msg = mqtt::MessageBuilder::new()
        .topic(format!("homeassistant/sensor/{}/humidity/config", device))
        .payload(serde_json::to_string(&humidity_message).unwrap())
        .qos(1)
        .finalize();

    let rssi_msg = mqtt::MessageBuilder::new()
        .topic(format!("homeassistant/sensor/{}/rssi/config", device))
        .payload(serde_json::to_string(&signal_strength).unwrap())
        .qos(1)
        .finalize();

    if let Err(e) = cli.publish(temperature_msg) {
        println!("Error sending message: {:?}", e);
    }
    if let Err(e) = cli.publish(humidity_msg) {
        println!("Error sending message: {:?}", e);
    }
    if let Err(e) = cli.publish(rssi_msg) {
        println!("Error sending message: {:?}", e);
    }

}

fn send_data_message(data_message: DataMessage, device: &String, cli: &Client){

    let data_msg = mqtt::MessageBuilder::new()
        .topic(format!("homeassistant/sensor/{}/state", device))
        .payload(serde_json::to_string(&data_message).unwrap())
        .qos(1)
        .finalize();

    if let Err(e) = cli.publish(data_msg) {
        println!("Error sending message: {:?}", e);
    }
}

fn process_manufacturer_data(fields: &Vec<BTField>, rssi: i8) -> Option<DataMessage> {
    for field in fields {
        if field.field_type == 0xff {
            println!("{:?}", field.data.len());
            println!("{:?}", field.data);
            return if field.data.len() == 20 {
                let mut temperature = (field.data[12] as u16 + (field.data[13] as u16 * 0x100)) as f32 * 0.0625;
                let mut humidity = (field.data[14] as u16 + (field.data[15] as u16 * 0x100)) as f32 * 0.0625;
                if temperature > 4000.0 {
                    temperature = -1.0 * (4096.0 - temperature)
                }
                println!("Temperature is {:?}", temperature);
                println!("Humidity is {:?}", humidity);
                let data_message = DataMessage {
                    temperature,
                    humidity,
                    rssi
                };
                Some(data_message)
            } else {
                None
            }
        }
    }
    return None;
}

fn process_field(field_data: Bytes) -> BTField {
    return BTField{
        field_type: field_data[0],
        data: field_data.slice(1..field_data.len())
    }
}

fn get_fields(eid: Bytes) -> Vec<BTField> {
    let mut processed_len = 0;
    let mut all_fields = Vec::new();
    while processed_len < eid.len() {
        let this_field_length = eid[processed_len];
        //println!("Field length {:?}", this_field_length);
        processed_len += 1;
        all_fields.push(process_field(eid.slice(processed_len..processed_len + (this_field_length as usize))));
        processed_len += this_field_length as usize;
    }
    return all_fields;
}

fn is_thermobeacon(fields: &Vec<BTField>) -> bool{
    for item in fields {
        match item.field_type{
            0x09 => {
                if &item.data[..] == b"ThermoBeacon"{
                    return true;
                } else {
                    println!("Name {:?}", item.data)
                }
            },
            _ => ()
        }
    }
    return false;
}


#[derive(StructOpt)]
struct Cli {
    /// The mqtt server to send data too
    mqtt_server: String,
    /// The credentials to connect to the mqtt server with
    username: String,
    password: String
}

#[async_std::main]
pub async fn main() -> core::result::Result<(), Box<dyn Error>> {

    let args = Cli::from_args();
    println!("{:?}", args.mqtt_server);
    let create_opts = mqtt::CreateOptionsBuilder::new()
    .server_uri(args.mqtt_server)
    .client_id("TempSensorHub-3")
    .finalize();

    let cli = mqtt::Client::new(create_opts).unwrap_or_else(|err| {
        println!("Error creating the client: {:?}", err);
        process::exit(1);
    });

    let conn_opts = mqtt::ConnectOptionsBuilder::new()
    .user_name(args.username)
    .password(args.password)
    .keep_alive_interval(Duration::from_secs(20))
    .automatic_reconnect(Duration::new(5,0), Duration::new(25,0))
    .clean_session(true)
    .finalize();

    if let Err(e) = cli.connect(conn_opts) {
        println!("Unable to connect:\n\t{:?}", e);
        process::exit(1);
    }

    let mut client = BlueZClient::new().unwrap();

    let controllers = client.get_controller_list().await?;

    // find the first controller we can power on
    let (controller, info) = controllers
        .into_iter()
        .filter_map(|controller| {
            let info = block_on(client.get_controller_info(controller)).ok()?;
            if info.supported_settings.contains(ControllerSetting::Powered) {
                Some((controller, info))
            } else {
                None
            }
        })
        .nth(0)
        .expect("no usable controllers found");

    if !info.current_settings.contains(ControllerSetting::Powered) {
        println!("powering on bluetooth controller {}", controller);
        client.set_powered(controller, true).await?;
    }

    // scan for some devices
    // to do this we'll need to listen for the Device Found event

    client
        .start_discovery(
            controller,
            AddressTypeFlag::BREDR | AddressTypeFlag::LEPublic | AddressTypeFlag::LERandom,
        )
        .await?;

    // just wait for discovery forever
    loop {
        // process() blocks until there is a response to be had
        let response = client.process().await?;

        match response.event {
            Event::DeviceFound {
                address,
                address_type,
                flags,
                rssi,
                eir_data,
                ..
            } => {
                let all_fields = get_fields(eir_data);
                if is_thermobeacon(&all_fields) {
                    let subbed_address = address.clone().to_string().replace(":", "-");
                    println!("Address: {:?}", subbed_address);
                    send_discover_message(&cli, &subbed_address);
                    match process_manufacturer_data(&all_fields, rssi) {
                        Some(x) => send_data_message(x, &subbed_address, &cli),
                        None => ()
                    }
                }
            }
            Event::Discovering {
                discovering,
                address_type,
            } => {
                println!("discovering: {} {:?}", discovering, address_type);

                // if discovery ended, turn it back on
                if !discovering {
                    client
                        .start_discovery(
                            controller,
                            AddressTypeFlag::BREDR
                                | AddressTypeFlag::LEPublic
                                | AddressTypeFlag::LERandom,
                        )
                        .await?;
                }
            }
            _ => (),
        }

        sleep(Duration::from_millis(50)).await;
    }
}
